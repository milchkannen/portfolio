import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';

//
// Material Design
//
// import { MdListModule } from '@angular2-material/list';
// import { MdCardModule } from '@angular2-material/card';
import { MdToolbarModule } from '@angular2-material/toolbar';
// import { MdGridListModule } from '@angular2-material/grid-list';
//import { MdButtonModule } from '@angular2-material/button';
// import { MdInputModule } from '@angular2-material/input';
import { MdIconModule } from '@angular2-material/icon';
// import { MdMenuModule } from '@angular2-material/menu';


@NgModule({
    imports: [
        BrowserModule,
        MdToolbarModule,
//        MdButtonModule,
        MdIconModule
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})



export class AppModule { }
