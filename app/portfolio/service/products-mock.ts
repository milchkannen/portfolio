import { Product } from './product';

export const PRODUCTS: Product[] = [
    {
        id: 0,
        title: 'First Picture',
        text: 'Nice try of bla bla',
        url: 'api/media/blechdose-lampe.jpg'
    }, {
        id: 1,
        title: 'Second Picture',
        text: 'Another try of bla bla',
        url: 'api/media/buch-memo.jpg'
    }, {
        id: 2,
        title: 'Third Picture',
        text: 'Last try of bla bla',
        url: 'api/media/werkzeugkiste-notebooktisch.jpg'
    }
];
