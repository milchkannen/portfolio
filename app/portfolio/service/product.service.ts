import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Product } from './product';


@Injectable()
export class ProductService {
    private productsUrl = 'app/products';       // URL to web api

    constructor(private http: Http) { }

    getProducts() {
        return this.http.get(this.productsUrl)
            .toPromise()
            .then(response => response.json().data as Product[])
            .catch(this.handleError);

    }

    getProduct(id: number) {
        return this.getProducts()
            .then(products => products.find(product => product.id === id));
    }

    save(product: Product): Promise<Product> {
        if (product.id) {
            return this.put(product);
        }
        return this.post(product);
    }

    delete(product: Product) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.productsUrl}/${product.id}`;

        return this.http
            .delete(url, { headers: headers })
            .toPromise()
            .catch(this.handleError);
    }

    // Add new Product
    private post(product: Product): Promise<Product> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http
            .post(this.productsUrl, JSON.stringify(product), { headers: headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    // Update existing Product
    private put(product: Product) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.productsUrl}/${product.id}`;

        return this.http
            .put(url, JSON.stringify(product), { headers: headers })
            .toPromise()
            .then(() => product)
            .catch(this.handleError);
    }

    // in case of an error
    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}
