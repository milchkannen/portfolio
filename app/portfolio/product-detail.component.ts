import { Component, EventEmitter, Input, OnInit, OnDestroy, Output } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

// import Product class (schema)
import { Product } from './service/product';

// import the main product for the start page
import { ProductService } from './service/product.service';



@Component({
    selector: 'portfolio-detail',
    template: `
    <div *ngIf="product">
        <div class="pf-card">
            <img [src]="product.url" class="pf-card-fullsize">
            <div class=pf-card-information>
                <md-card-title class="md-display-1">{{product.title}}</md-card-title>
                <p class="md-headline">{{product.text}}</p>
            </div>
        </div>
    </div>
  `
})
export class ProductDetailComponent implements OnInit, OnDestroy {
    @Input() product: Product;
    @Output() close = new EventEmitter();
    error: any;
    sub: any;
    navigated = false; // true if navigated here
    constructor(
        private productService: ProductService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            if (params['id'] !== undefined) {
                let id = +params['id'];
                this.navigated = true;
                this.productService.getProduct(id)
                    .then(product => this.product = product);
            } else {
                this.navigated = false;
                this.product = new Product();
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }


}
