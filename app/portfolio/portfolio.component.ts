import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';

// import Product class (schema)
import { Product } from './service/product';

// import the main product for the start page
import { ProductService } from './service/product.service';



@Component({
    selector: 'portfolio-start',
    template: `
            <div class="pf-card">
                <img [src]="currentImage" class="pf-card-fullsize">
                <div class=pf-card-information>
                    <md-card-title class="md-display-1">{{portfolio.title}}</md-card-title>
                    <p class="md-headline">{{portfolio.subtitle}}</p>
                </div>
            </div>
        `,
    providers: [ProductService]
})

export class PortfolioComponent {


    // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

    portfolio = {
        'title': 'Mach etwas Gutes aus Abfall',
        'subtitle': 'Willkommen bei Milchkannen — DIY Recycling',
    }



    // load the product images from the api service
    // and set the current (main/detail) product image

    constructor(
//        private router: Router,
        private productService: ProductService) { }

// delete the next line later!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    currentImage: string = 'api/media/digital-typewriter.jpg';
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --







    // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --


}
