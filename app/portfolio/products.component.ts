import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { Product }                from './service/product';
import { ProductService }         from './service/product.service';

import { ProductDetailComponent } from './product-detail.component';

@Component({
  selector: 'my-products',
  template: '<p>products component</p>)',
//  styleUrls:  ['app/products.component.css'],
})

export class ProductsComponent implements OnInit {
  products: Product[];
  selectedProduct: Product;
  addingProduct = false;
  error: any;
  constructor(
    private router: Router,
    private productService: ProductService) { }
  getProducts() {
    this.productService
        .getProducts()
        .then(products => this.products = products)
        .catch(error => this.error = error);
  }
  addProduct() {
    this.addingProduct = true;
    this.selectedProduct = null;
  }
  close(savedProduct: Product) {
    this.addingProduct = false;
    if (savedProduct) { this.getProducts(); }
  }
  deleteProduct(product: Product, event: any) {
    event.stopPropagation();
    this.productService
        .delete(product)
        .then(res => {
          this.products = this.products.filter(h => h !== product);
          if (this.selectedProduct === product) { this.selectedProduct = null; }
        })
        .catch(error => this.error = error);
  }
  ngOnInit() {
    this.getProducts();
  }
  onSelect(product: Product) {
    this.selectedProduct = product;
    this.addingProduct = false;
  }
  gotoDetail() {
    this.router.navigate(['/detail', this.selectedProduct.id]);
  }
}
