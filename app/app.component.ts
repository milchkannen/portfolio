import { Component } from '@angular/core';
@Component({
  selector: 'portfolio',
  template: `
    <md-toolbar class="transparent" [color]="primary">
        <!-- Left Aligned Text (Title) -->
        <span class="pf-title">
            <a href="{{portfolio.url}}"><img class="pf-logo" src="./api/media/{{portfolio.logo}}"/></a>
        </span>
        <!-- This fills the remaining space of the current row -->
        <span class="fill-remaining-space"></span>
        <!-- Right Aligned Text (Navigation Menu Button) -->
        <span>
        </span>
    </md-toolbar>
    <div class="start">
        <img [src]="currentImage" class="pf-card-fullsize">
    </div>
    `
})

export class AppComponent {

    portfolio = {
        'url': '',
        'logo': 'milchkannen-stamp.png'
    }
    currentImage: string = 'api/media/digital-typewriter.jpg';



}
