/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function(global) {
    var packages = {
        app: {
            main: './main.js',
            defaultExtension: 'js'
        },
        'rxjs': {
            defaultExtension: 'js'
        },
        'angular2-in-memory-web-api': {
            main: './index.js',
            defaultExtension: 'js'
        }
    };
    var map = {
        app: 'app',
        'rxjs': 'npm:rxjs',
        'angular2-in-memory-web-api': 'npm:angular2-in-memory-web-api'
    };

    var ngPackageNames = [
        'core',
        'common',
        'compiler',
        'platform-browser',
        'platform-browser-dynamic',
        'http',
        'router',
        'forms'
    ];
    ngPackageNames.forEach(function(ngPackage) {
        map['@angular/' + ngPackage] = 'npm:@angular/' + ngPackage + '/bundles/' + ngPackage + '.umd.js';
    });

    var mdPackageNames = [
        'core',
        'button',
        'card',
        'input',
        'sidenav',
        'toolbar',
        'icon',
    ];
    mdPackageNames.forEach(function(mdPackage) {
        map['@angular2-material/' + mdPackage] = 'npm:@angular2-material/' + mdPackage;
        packages['@angular2-material/' + mdPackage] = {
            format: 'cjs',
            defaultExtension: 'js',
            main: mdPackage + '.js'
        };
    });

    System.config({
        paths: {
            // paths serve as alias
            'npm:': 'node_modules/'
        },
        // map tells the System loader where to look for things
        map: map,
        // packages tells the System loader how to load when no filename and/or no extension
        packages: packages
    });
})(this);
